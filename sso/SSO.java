package util.sso;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ksheppard on 9/4/2015.
 */
public class SSO {
    private SqlRow session;

    public static SSO getCurrentSession(String token) {
        SqlRow session = Ebean.getServer("sso").createSqlQuery("select * from session where token = :token")
                .setParameter("token", token)
                .findUnique();
        return session.isEmpty() ? null : new SSO(session);
    }

    private SSO(SqlRow session) {
        this.session = session;
    }

    public List<String> getGroups() {
        return Arrays.asList(session.getString("groups").split(","));
    }

    public String getUsername() {
        return session.getString("username");
    }
}
