package util;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: ksheppard
 * Date: 1/14/14
 * Time: 11:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class Generator {
    public static String generateToken(int length) {
        Random random = new Random();
        char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmonpqrstuvwxyz1234567890-_".toCharArray();
        char[] buf = new char[length];
        for(int i=0; i<length; i++) {
            buf[i] = chars[random.nextInt(chars.length)];
        }
        return new String(buf);
    }

    public static String generateFileSuffix(String prefix) {
        String dateFormat = "yyyy_MM_dd_H_m_s";
        String fileName = prefix+new SimpleDateFormat(dateFormat).format(new Date())+".csv";
        return fileName;
    }
}
