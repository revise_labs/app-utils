package util;

import org.quartz.*;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;


/**
 * Created by ksheppard on 8/12/2014.
 */
public class CronJob {
    private String name;
    private Class<? extends Job> job;
    private Scheduler scheduler;
    private CronScheduleBuilder schedule;

    /**
     * Constructor method
     * @param name Name of the job
     * @param job A class that extends org.quartz.job
     * @param schedule A CRON schedule e.g. CronScheduleBuilder.schedule(seconds, minutes, hours, day-of-month, month, day-of-week, year)
     * @param scheduler StdSchedulerFactory.getDefaultScheduler() usually
     */
    public CronJob(String name, Class<? extends Job> job, CronScheduleBuilder schedule, Scheduler scheduler) {
        this.name = name;
        this.job = job;
        this.schedule = schedule;
        this.scheduler = scheduler;
    }

    public void schedule() {
        JobDetail detail = newJob(job).withIdentity(name).build();
        Trigger trigger = newTrigger()
                .withIdentity(name + "-trigger")
                .withSchedule(schedule)
                .build();
        try { scheduler.scheduleJob(detail, trigger); }
        catch(SchedulerException e) {
            e.printStackTrace();
        }

    }

}
