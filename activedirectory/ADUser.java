package util.activedirectory;

import org.apache.commons.lang3.StringUtils;
import org.jboss.netty.util.internal.StringUtil;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ksheppard on 8/11/2015.
 */
public class ADUser {
    private String fullName;
    private String email;
    private List<String> groups;
    private String username;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getGroups() {
        return groups;
    }

    public String getGroupsAsString() {
        return StringUtils.join(groups, ",");
    }

    public void setGroups(NamingEnumeration groups) throws NamingException {
        this.groups = new ArrayList<>();
        while(groups.hasMore()) {
            String groupName = groups.next().toString()
                    .split(",")[0]
                    .split("=")[1];
            this.groups.add(groupName);
        }
        //System.out.println(this.groups.toString());

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "ADUser {" +
                "fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", groups=" + groups +
                '}';
    }
}

