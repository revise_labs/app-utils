package util.activedirectory;


import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import static javax.naming.Context.*;

/**
 * Created with IntelliJ IDEA.
 * User: ksheppard
 * Date: 11/13/13
 * Time: 5:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class ActiveDirectory {
    private Hashtable<String, String> env;
    private String username;
    private String password;

    public ActiveDirectory() {
        env = new Hashtable<>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.PROVIDER_URL, "ldap://posads01:389");
    }

    public boolean authenticate(String username, String password) {
        this.username = username;
        this.password = password;
        String principal = username+"@mpu.gov.tt";
        env.put(Context.SECURITY_PRINCIPAL, principal);
        env.put(Context.SECURITY_CREDENTIALS, password);

        try {
            new InitialDirContext(env);
            return true;
        } catch (NamingException ex) {
            ex.printStackTrace();
            return false;
        }

    }

    public ADUser getUserDetails() {
        if(authenticate(username, password)) {
                try {
                    DirContext ctx = new InitialDirContext(env);

                    //Create a search control
                    SearchControls searchControls = new SearchControls();

                    //Specify the attributes that we want returned from the search
                    String[] attrs = {"name", "memberOf", "mail", "userPrincipalName", "samAccountName"};
                    searchControls.setReturningAttributes(attrs);

                    //Search the staff portion of the domain
                    String searchBase = "OU=MPU Staff,DC=mpu,DC=gov,DC=tt";

                    //Seach all subtrees (departments)
                    searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

                    //Look for a *user* whose *sAMAccountName* is whatever username is
                    String searchFilter = "(&(objectClass=user) (sAMAccountName="+username+"))";

                    //Perform the search
                    NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchControls);

                    if(answer.hasMoreElements()) {
                        SearchResult result = answer.next();
                        Attributes attributes = result.getAttributes();
                        ADUser user = new ADUser();
                        user.setFullName((String) attributes.get("name").get());
                        user.setEmail((String) attributes.get("mail").get());
                        user.setUsername((String) attributes.get("samAccountName").get());
                        user.setGroups(attributes.get("memberOf").getAll());
                        return user;
                    }

                } catch (NamingException ex) {
                    ex.printStackTrace();
                    return null;
                }

        }
        return null;
    }

}
