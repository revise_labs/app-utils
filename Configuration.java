package util;

import play.Play;

/**
 * Created by ksheppard on 4/15/2015.
 */
public class Configuration {

    public static Object get(String value) {
        return Play.application().configuration().getString(value);
    }

    public static String dateFormat = "yyyy-MM-dd";
}
